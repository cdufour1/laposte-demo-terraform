terraform {
  backend "http" {
  }
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.1"
    }
  }
}

variable "gitlab_access_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_access_token
}

data "gitlab_project" "example" {
  id = 36475382
}

resource "gitlab_project_variable" "example" {
  project   = data.gitlab_project.example.id
  key       = "demo_variable"
  value     = "ciao"
}